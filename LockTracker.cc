// ----------------------------------------------------------------------
// File: LockTracker.cc
// Author: Georgios Bitzes - CERN
// ----------------------------------------------------------------------

#include "LockTracker.hh"
#include <fcntl.h>
#include <unistd.h>

std::ostream& operator<< (std::ostream &os, const ByteRange &range) {
  os << "[" << range.start() << ", " << range.end() << ")";
  return os;
}

std::ostream& operator<< (std::ostream &os, const Lock &lock) {
  os << lock.range() << " on pid " << lock.pid() << std::endl;
  return os;
}

void LockSet::add(const Lock &l) {
  Lock newlock(l);

  // Absorb any overlapping ranges, removing the old ones
  auto it = locks.begin();
  while(it != locks.end()) {
    if(newlock.absorb(*it)) {
      it = locks.erase(it);
    }
    else {
      it++;
    }
  }

  // Append the consolidated superlock
  locks.push_back(newlock);
}

bool LockSet::conflict(const Lock &l) const {
  auto it = locks.begin();
  while(it != locks.end()) {
    if(it->pid() != l.pid() && l.range().overlap(it->range())) return true;
    it++;
  }
  return false;
}

bool LockSet::overlap(const Lock &l) const {
  auto it = locks.begin();
  while(it != locks.end()) {
    if(l.overlap(*it)) return true;
    it++;
  }
  return false;
}

bool LockSet::overlap(const ByteRange &br) const {
  auto it = locks.begin();

  while(it != locks.end()) {
    if(br.overlap(it->range())) return true;
    it++;
  }
  return false;
}

void LockSet::remove(const Lock &l) {
  std::vector<Lock> queued;

  auto it = locks.begin();
  while(it != locks.end()) {
    std::vector<Lock> newlocks = it->minus(l);

    if(newlocks.size() == 0) {
      it = locks.erase(it);
      continue;
    }

    if(newlocks.size() >= 1) {
      *it = newlocks[0];
    }

    if(newlocks.size() == 2) {
      queued.push_back(newlocks[1]);
    }

    it++;
  }

  // Cannot add new items to vector while iterating, as it invalides the iterators.
  // Store the items in a queue and add them later.
  for(size_t i = 0; i < queued.size(); i++) {
    locks.push_back(queued[i]);
  }
}

size_t LockSet::nlocks() {
  return locks.size();
}

size_t LockSet::nlocks(pid_t pid) {
  size_t res = 0;
  auto it = locks.begin();
  while(it != locks.end()) {
    if(it->pid() == pid) res++;
    it++;
  }
  return res;
}

int LockTracker::getlk(pid_t pid, struct flock* lock) {
  std::lock_guard<std::mutex> guard(mtx);

  if(lock->l_type == F_UNLCK) {
    // TODO signal warning, should not happen (?)
    return 1;
  }

  return canLock(pid, lock);
}

int LockTracker::setlk(pid_t pid, struct flock* lock, int sleep) {
  if(!sleep) return addLock(pid, lock);

  while(!addLock(pid, lock)) {
    // TODO wait on condition variable?
    ::sleep(1);
  }
  return 1;
}

bool LockTracker::canLock(pid_t pid, struct flock* f_lock) {
  Lock lock(ByteRange(f_lock->l_start, f_lock->l_len), pid);

  // Can always unlock
  if(f_lock->l_type == F_UNLCK) return true;

  // Are there any exclusive locks right now?
  if(wlocks.conflict(lock)) return false;

  // If this is a read lock, we can lock
  if(f_lock->l_type == F_RDLCK) {
    return true;
  }

  // If this is a write lock, we can lock only if there are no read locks
  if(f_lock->l_type == F_WRLCK) {
    return rlocks.conflict(lock);
  }

  // TODO raise warning, should never reach this point
  return false;
}

bool LockTracker::addLock(pid_t pid, struct flock* f_lock) {
  std::lock_guard<std::mutex> guard(mtx);

  Lock lock(ByteRange(f_lock->l_start, f_lock->l_len), pid);

  // Unlock?
  if(f_lock->l_type == F_UNLCK) {
    rlocks.remove(lock);
    wlocks.remove(lock);
    return true;
  }

  // Exclusive lock?
  if(f_lock->l_type == F_WRLCK) {
    // Conflict with any read locks?
    if(rlocks.conflict(lock)) return false;

    // Conflict with any write locks?
    if(wlocks.conflict(lock)) return false;

    // Add write lock
    wlocks.add(lock);

    // It could be that the process is converting a read lock into a write.
    // Remove any read locks on the same region.
    rlocks.remove(lock);

    return true;
  }

  // Read lock?
  if(f_lock->l_type == F_RDLCK) {
    // Conflict with any write locks?
    if(wlocks.conflict(lock)) return false;

    // Add read lock
    rlocks.add(lock);

    // It could be that the process is converting a write lock into a read.
    // Remove any write locks on the same region.
    wlocks.remove(lock);

    return true;
  }

  // TODO raise warning, should never reach this point
  std::cerr << "WARNING, something is wrong" << std::endl;
  return false;
}
