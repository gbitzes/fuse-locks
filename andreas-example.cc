int
/* -------------------------------------------------------------------------- */
metad::getlk(fuse_req_t req, shared_md md, struct flock* lock)
/* -------------------------------------------------------------------------- */
{
  int rc = 0;
  if (!md->locks_remote())
  { // we let the kernel do local locking rc = ENOSYS; }
  else
  {
    for (auto lk = md->locktable.begin(); lk != md->locktable.end(); ++lk)
    {
      if (lk->l_pid != fuse_req_ctx (req)->pid)
      {
        // check for locks which we don't hold
        if (lk->l_type == F_WRLCK)
        {
          if (lk->l_len == 0)
          { // full file lock *lock = *lk; return 0; }

          if ( ((lock->l_start >= lk->l_start) &&
          (lock->l_start < (lk->l_start + lk->l_len))) ||
          ((lock->l_start < lk->l_start) &&
          ((lock->l_start + lock->l_len) > lk->l_start))
        )
        { // range overlap *lock = *lk; return 0; }
      }
    }
  }
  lock->l_type = F_UNLCK;
}
return rc;
}

/* -------------------------------------------------------------------------- */
int
/* -------------------------------------------------------------------------- */
metad::setlk(fuse_req_t req, shared_md md, struct flock* lock, int sleep)
/* -------------------------------------------------------------------------- */
{
  int rc = 0;
  if (!md->locks_remote())
  { // we let the kernel do local locking rc = ENOSYS; }
  else
  {
    if (lock->l_type == F_UNLCK)
    {
      std::vector<struct flock> addpieces;
      for (auto lk = md->locktable.begin(); lk != md->locktable.end(); ++lk)
      {
        eos_static_debug("EXAMINE(unlock) lock-start=%lu lock-len=%lu lock-type=%s",
        lk->l_start, lk->l_len,
        (lk->l_type == F_RDLCK) ? "rdlock" : "wrlock");
        if ( ((lock->l_start >= lk->l_start) &&
        (lock->l_start < (lk->l_start + lk->l_len))) ||
        ((lock->l_start < lk->l_start) &&
        ((lock->l_start + lock->l_len) > lk->l_start))
      )
      {
        if (lk->l_pid != fuse_req_ctx (req)->pid)
        { // we don't own this region rc = EACCES; break; }
        // range overlap - we might split the region into two pieces
        if ( (lock->l_start + lock->l_len) < (lk->l_start + lk->l_len))
        { // we split into two pieces struct flock newpiece; newpiece = *lk; // shrink the initial piece lk->l_len = lock->l_start - lk->l_start; // add a left-over piece newpiece.l_start = lock->l_start + lock->l_len; newpiece.l_len -= (newpiece.l_start - lk->l_start); addpieces.push_back(newpiece); }
        else
        { // we shrink this piece lk->l_len = lock->l_start - lk->l_start; }
      }
      else
      {
        if ( (lock->l_start == lk->l_start) &&
        (lock->l_len == lk->l_len) )
        { eos_static_debug("UNLOCK lock-start=%lu lock-len=%lu lock-type=%s", lk->l_start, lk->l_len, (lk->l_type == F_RDLCK) ? "rdlock" : "wrlock"); md->locktable.erase(lk); rc = 0; break; }
      }
    }
    // add all additional pieces now
    for (auto lk = addpieces.begin(); lk != addpieces.end(); ++lk)
    { md->locktable.push_back(*lk); }
  }
  else
  {
    for (int retry = 0; retry < sleep ? 86400 : 1; ++retry)
    {
      for (auto lk = md->locktable.begin(); lk != md->locktable.end(); ++lk)
      {
        eos_static_debug("EXAMINE() lock-start=%lu lock-len=%lu lock-type=%s",
        (lock->l_type == F_RDLCK) ? "rdlock" : "wrlock",
        lk->l_start, lk->l_len,
        (lk->l_type == F_RDLCK) ? "rdlock" : "wrlock");
        if (lk->l_type == F_WRLCK)
        {
          if ( ((lock->l_start >= lk->l_start) &&
          (lock->l_start < (lk->l_start + lk->l_len))) ||
          ((lock->l_start < lk->l_start) &&
          ((lock->l_start + lock->l_len) > lk->l_start))
        )
        {
          // range overlap
          if (lk->l_pid != fuse_req_ctx (req)->pid)
          { // we are not the owner rc = EACCES; break; }
          else
          {
            // we are the owner
            if (lock->l_start < lk->l_start)
            { // extend this lock lk->l_start = lock->l_start; }
            if ( (lock->l_start + lock->l_len) > (lk->l_start + lk->l_len))
            { lk->l_len += (lk->l_start + lk->l_len - lock->l_start - lock->l_len); }
          }
        }
        { // add a new lock md->locktable.push_back(*lock); break; }
      }
    }
    if (rc && sleep)
    { XrdSysTimer sleeper; sleeper.Wait(250); }
    else
    break;
  }
}
}
return rc;
}}}
